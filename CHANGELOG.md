# Change Log  

## [2017-09-15]  
+ **Added**  
	- [62-Unique-Paths](https://leetcode.com/problems/unique-paths/description/)  
	- [63-Unique-Paths-II](https://leetcode.com/problems/unique-paths-ii/description/)  
	- [414-Third-Maximum-Number](https://leetcode.com/problems/third-maximum-number/description/)  

## [2017-09-13]  
+ **Added**  
	- `Course-Schedule` for [problem 207](https://leetcode.com/problems/course-schedule/description/)    
	- `Course-Schedule-II` for [problem 207](https://leetcode.com/problems/course-schedule-ii/description/)  
	- `Binary-Tree-Level-Order-Traversal-II` for [problem 107](https://leetcode.com/problems/binary-tree-level-order-traversal-ii/description/)  
	- `Average-of-Levels-in-Binary-Tree` for [problem 637](https://leetcode.com/problems/average-of-levels-in-binary-tree/description/)  
	- `Convert-Sorted-Array-to-Binary-Search-Tree` for [problem 108](https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/description/)  
	- `Minimum-Depth-of-Binary-Tree` for [problem 111](https://leetcode.com/problems/minimum-depth-of-binary-tree/description/)  
	- `Pascals-Triangle` for [problem 118](https://leetcode.com/problems/pascals-triangle/description/)  
	- `Pascals-Triangle-II` for [problem 119](https://leetcode.com/problems/pascals-triangle-ii/description/)  
	- `Valid-Palindrome` for [problem 125](https://leetcode.com/problems/valid-palindrome/description/)  
	- `Linked-List-Cycle` for [problem 141](https://leetcode.com/problems/linked-list-cycle/description/)  
	- `Majority-Element` for [problem 169](https://leetcode.com/problems/majority-element/description/)  
	- `Rotate-Array` for [problem 189](https://leetcode.com/problems/rotate-array/description/)  
	- `Rotate-List` for [problem 61](https://leetcode.com/problems/rotate-list/description/)  

## [2017-09-12]  
+ **Added**  
	- `Longest-Palindromic-Substring` for [problem 5](https://leetcode.com/problems/longest-palindromic-substring/description/)  

## [2017-09-09]  
+ **Added**  
	- `Range-Addition-II` for [problem 598](https://leetcode.com/problems/range-addition-ii/description/)  
	- `Binary-Tree-Paths` for [problem 257](https://leetcode.com/problems/binary-tree-paths/description/)  
	- `Path-Sum-II` for [problem 113](https://leetcode.com/problems/path-sum-ii/description/)  
	- `Path-Sum` for [problem 112](https://leetcode.com/problems/path-sum/description/)   
	- `Sum-Root-to-Leaf-Numbers` for [problem 129](https://leetcode.com/problems/path-sum-ii/description/)   

## [2017-08-21]  
+ **Added**  
	- `Image-Smoother` for [problem 661](https://leetcode.com/problems/image-smoother/description/)  

## [2017-08-12]  
+ **Added**  
	- `Range-Sum-Query-2D-Immutable` for  [problem 304](https://leetcode.com/problems/range-sum-query-2d-immutable/description/)  
	- An alternative solution to `Nth-Digit` for [problem 400](https://leetcode.com/problems/nth-digit/description/)   

## [2017-08-11]  
+ **Added**  
	- `Best-Time-to-Buy-and-Sell-Stock-II` for [problem](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/description/)     
	- `Best-Time-to-Buy-and-Sell-Stock-with-Cooldown` for [problem](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-cooldown/description/)   
	- `Best-Time-to-Buy-and-Sell-Stock` for [problem](https://leetcode.com/problems/best-time-to-buy-and-sell-stock/description/)  
	- `Range-Sum-Query-Immutable` for [problem](https://leetcode.com/problems/range-sum-query-immutable/description/)  
	- `Set-Mismatch` for [problem 645](https://leetcode.com/problems/set-mismatch/description/)   
	- `Sum-of-Square-Numbers` for [problem 633](https://leetcode.com/problems/sum-of-square-numbers/description/)  
	- `Nth-Digit` for [problem 400](https://leetcode.com/problems/nth-digit/description/)  

## [2017-08-08]  
+ **Added**  
	- `Pow(x,n)` implements an operation calculating the `x` to the power of `n`  

### [2017-06-11]  
+ **Added**  
	- `README.md`  
	- `Add-Two-Number`  
	- `Merge-Two-Sorted-Lists`  
	- `Palindrome-Linked-List`  
	- `Palindrome-Number`  
	- `Remove-Linked-List-Elements`  
	- `Reverse-Integer`  
	- `Reverse-Linked-List`  
	- `Reverse-Linked-List-II`  
	- `StringToInteger`  
	- `Two-Sum`  
	- `Valid-Parentheses`  
